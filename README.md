# healthRecordsMysql

Validation is set up but incomplete -- so be careful when testing<br />
Please remember to click the date button and include a gender (or it will fail) when you create or edit a patient <br />
Kindly do not make hCard number a duplicate key when testing make sure different number-- it will fail <br />
Validation is constructed and available but needs to be impmlemented to be added for this to front end <br />
After changing a patient hCard number(primary key) do not refresh page or it will fail -- click return to patients message flashed to top <br />
Primary key names across tables need to be re-evaluated and given common consistent names <br />
All tables need created and updated values with default set to timestamp/current datetime <br />
Record insertion needs to update the page upon insertion -- currently requires manual refresh <br />
Record page needs styling <br />
Images need to be integrated into the sytem CRUD functionality required including storage and retrieval needs to be done <br />
Console.log statements need to be removed for production <br />
We struggled with deciding what to do with patients and records -- deciding to update the postedBy to administrator as otherwise the system fails -- when a new doctor is hired, patients can then be moved over to that doctor -- role needs to be updated for this query<br />
Formatting for date and phone number is done using SQL in file summary.js : 15 -- this was an proof of functionality with SQL as normally done front end.<br />
Some routing is awkward -- the patient summary page can only be reached by entering the patient id -- after entering a record -- returns to find patient and not single patient -- routing needs to be re thought through<br />
Select record by patientID is not working -- same records show for all patient ... needs to be fixed on front end and select fy patient_id on backend<br />

## TO RUN

1. Set up MySQL by doing the following in the terminal or using Workbench

```
mysql -u root -p
enter password

USE mysql;
CREATE USER 'newuser'@'localhost' IDENTIFIED WITH mysql_native_password BY '#{2gaM7N{*6>}aTH';
GRANT ALL PRIVILEGES ON *.* TO 'newuser'@'localhost';
flush privileges;

CREATE DATABASE health_records;
USE health_records;
```

2. Mysql dump file provided called `sql.sql` in the root folder to create the tables and patients / records
3. `cd backend` and `npm install`
4. `cd frontend` and `npm install`
5. `npm run dev` (for backend testing)
6. `npm start` (for frontend)
7. `user1@user1.com`, `user2@user2.com` and `admin@admin.com` can all be used to log in. The password is 12345678 for all.
8. Update a user role to 'admin' from the backend if needed to be an administrator. For example in MySQL: `UPDATE users SET role="admin" WHERE _id = "1";`

## SQL QUERY LOG

See file called `sqlQueryLog.txt` in the root folder for a full SQL Query Log
