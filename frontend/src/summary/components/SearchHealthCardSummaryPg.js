import React, { useState } from 'react';
import './../Styles.css';
import { useHistory, withRouter} from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

const SearchHealthCardSummary = () => { 

    const[searchHealthCard, setSearchHealthCard] = useState({})
    const history = useHistory();

    const handleChange = (event) => {
        event.persist();
        setSearchHealthCard((prevState) => ({
            ...prevState,
            [event.target.name]: event.target.value,
        }));
    };

    const handleSubmit = (event) => {
        fetch("http://localhost:8000/api/patientsummary/search", {
            method: "post",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },

            body: JSON.stringify(searchHealthCard),
            }).then((response) => response.json());

            history.push(`/patientsummary/${searchHealthCard.hCard}`)
    };

    return(
        <div className="search-health-card col-12 col-lg-4 col-xl-3 p-0 margin-form">
            <form onSubmit={handleSubmit} className="form-horizontal pt-1">
                <div className="search-area input-group p-0">
                    <input
                        className="form-control"
                        type="text"
                        name="hCard"
                        value={searchHealthCard.hCard}
                        onChange={handleChange}
                        placeholder="Search Health Card Number" 
                        title="Search Health Card Number"
                    />
                    <button type="search-btn button" className="btn bg-custom-1">
                        <FontAwesomeIcon icon={faSearch} />
                    </button> 
                </div>
            </form>
        </div>     
    )
};

export default withRouter(SearchHealthCardSummary);