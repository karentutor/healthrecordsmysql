
exports.patientQueryJoin = 'p.hCard as patient_id, p.fName, p.lName, p.gender, p.dob, p.address, p.pNumber, p.email, p.information, p.postedBy, p.role';
exports.patientQuery = 'hCard as patient_id, fName, lName, gender, dob, address, pNumber, email, information, postedBy, role';
