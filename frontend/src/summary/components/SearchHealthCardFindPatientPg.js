import React, { useState } from 'react';
import { useHistory, withRouter } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

const SearchHealthCardMain = () => { 

    const[searchHealthCard, setSearchHealthCard] = useState({})
    const history = useHistory();

    const handleChange = (event) => {
        event.persist();
        setSearchHealthCard((prevState) => ({
            ...prevState,
            [event.target.name]: event.target.value,
        }));
    };

    const handleSubmit = (event) => {
        fetch("http://localhost:8000/api/patientsummary/search", {
            method: "post",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },

            body: JSON.stringify(searchHealthCard),
            }).then((response) => response.json());

            history.push(`/patientsummary/${searchHealthCard.hCard}`)
    };

    return(
        <div className="search-health-card p-0 margin-form row mb-4 px-2">
            <form onSubmit={handleSubmit} className="form-horizontal">
                <h3 className="h4 text-white pl-0 ml-0 col-12 text-left">Search Health Card Number</h3>
                <div className="search-area input-group p-0">
                    <input
                        className="form-control"
                        type="text"
                        name="hCard"
                        value={searchHealthCard.hCard}
                        onChange={handleChange}
                        placeholder="Enter Health Card Number" 
                        title="Search Health Card Number"
                    />
                    <button type="search-btn button btn-dark" className="btn bg-custom-3">
                        <FontAwesomeIcon icon={faSearch} />
                    </button> 
                </div>
            </form>
        </div>      

    )
};

export default withRouter(SearchHealthCardMain);