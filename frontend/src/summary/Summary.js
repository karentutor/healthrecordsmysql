import React, { useEffect, useState } from 'react';
// import logo from './../images/hospitalLogoCropped.png';
import { Tab, Row, Nav, Modal, Button } from 'react-bootstrap';
import './Styles.css'; 
import SearchHealthCardSummaryPage from './components/SearchHealthCardSummaryPg';
import ReadPatientInfoTop from './components/ReadPatientInfoTop';


const Summary = () => {

    
    const [one, aShow] = useState(false);
    const [two, cShow] = useState(false);
    const [three, conShow] = useState(false);
    const [four, diShow] = useState(false);
    const [five, enShow] = useState(false);
    const [six, immShow] = useState(false);
    const [seven, medShow] = useState(false);
    const [eight, olShow] = useState(false);
    const [nine, ovShow] = useState(false);
    const [ten, prShow] = useState(false);
    
    

    const allergyClose= () => aShow(false);
    const allergyShow= () => aShow(true);

    const careClose= () => cShow(false);
    const careShow= () => cShow(true);

    const conditionClose= () => conShow(false);
    const conditionShow= () => conShow(true);

    const diagnosticClose= () => diShow(false);
    const diagnosticShow= () => diShow(true);

    const encounterClose= () => enShow(false);
    const encounterShow= () => enShow(true);

    const immunizationClose= () => immShow(false);
    const immunizationShow= () => immShow(true);

    const medicationClose= () => medShow(false);
    const medicationShow= () => medShow(true);

    const oLClose= () => olShow(false);
    const oLShow= () => olShow(true);

    const oVClose= () => ovShow(false);
    const oVShow= () => ovShow(true);

    
    const procedureClose = () => prShow(false);
    const procedureShow = () => prShow(true);



  
    return(

        <>

            <div class="container-fluid summary">
            
                <header>
                    <div className="row heading-container">
                        {/* Logo and Name */}
                        <div className="col-12 col-lg-8 col-xl-9 pt-2">
                            {/* <img className="logo" src={logo} width={75} alt="Talens General Hospital Logo" /> */}
                            <h1>Talens General Hospital</h1>
                        </div>

                        {/* Search Box */}
                        <SearchHealthCardSummaryPage/>

                    </div>
                </header>

                <main>

                    {/* Patient Information Section at Top of Page */}
                    <ReadPatientInfoTop />

                    {/* STARTS - All Patient Medical Data - Includes Left-Side Tabs and Right-Side Data Displayed */}
                    <section className="patient-data">
                        <Tab.Container id="left-tabs-example" defaultActiveKey="first" className="patient-tabs nav flex-column nav-pills">                
                            <Row>

                                {/* STARTS - All Tabs - Displayed on the left */}
                                <section className="all-tabs col-xl-3 col-lg-4 col-12">
                                    <Nav variant="pills" className="flex-column py-3">

                                        {/* Search Box on Top of Tabs*/}
                                        {/* <Nav.Item>
                                            <Nav.Link eventKey="search" className="px-custom-1 py-custom-1">
                                                    <form action="true" autoComplete="off" className="form-horizontal px-0" method="post" acceptCharset="utf-8">
                                                        <div className="search-area input-group col-12 mb-0">
                                                            <input id="search-focus" type="search" className="form-control" placeholder="Search Patient Details" title="Search Patient Details" />
                                                            <button type="search-btn button" className="btn bg-custom-1">
                                                            <FontAwesomeIcon icon={faSearch} />
                                                            </button>
                                                        </div>
                                                    </form>
                                            </Nav.Link>
                                        </Nav.Item> */}

                                        {/* 1st Tab - AllergyIntolerance */}
                                        <Nav.Item className="navigation-link">
                                            <Nav.Link eventKey="first" className="d-flex justify-content-between">
                                                <span className="category">
                                                    AllergyIntolerance
                                                </span>
                                                <span className="circle">
                                                    2
                                                </span>
                                            </Nav.Link>
                                        </Nav.Item>

                                        {/* 2nd Tab - CarePlan */}
                                        <Nav.Item className="navigation-link">
                                            <Nav.Link eventKey="second" className="d-flex justify-content-between">
                                                <span className="category">
                                                    CarePlan
                                                </span>
                                                <span className="circle">
                                                    2
                                                </span>
                                            </Nav.Link>
                                        </Nav.Item>

                                        {/* 3nd Tab - Condition */}
                                        <Nav.Item className="navigation-link">
                                            <Nav.Link eventKey="third" className="d-flex justify-content-between">
                                                <span className="category">
                                                    Condition
                                                </span>
                                                <span className="circle">
                                                    2
                                                </span>
                                            </Nav.Link>
                                        </Nav.Item>

                                        {/* 4th Tab - DiagnositicReport */}
                                        <Nav.Item className="navigation-link">
                                            <Nav.Link eventKey="fourth" className="d-flex justify-content-between">
                                                <span className="category">
                                                    DiagnosticReport
                                                </span>
                                                <span className="circle">
                                                    2
                                                </span>
                                            </Nav.Link>
                                        </Nav.Item>

                                        {/* 5nd Tab - Encounter */}
                                        <Nav.Item className="navigation-link">
                                            <Nav.Link eventKey="fifth" className="d-flex justify-content-between">
                                                <span className="category">
                                                    Encounter
                                                </span>
                                                <span className="circle">
                                                    2
                                                </span>
                                            </Nav.Link>
                                        </Nav.Item>

                                        {/* 6nd Tab - Immunization */}
                                        <Nav.Item className="navigation-link">
                                            <Nav.Link eventKey="sixth" className="d-flex justify-content-between">
                                                <span className="category">
                                                    Immunization
                                                </span>
                                                <span className="circle">
                                                    2
                                                </span>
                                            </Nav.Link>
                                        </Nav.Item>

                                        {/* 7th Tab - MedicationOrder */}
                                        <Nav.Item className="navigation-link">
                                            <Nav.Link eventKey="seventh" className="d-flex justify-content-between">
                                                <span className="category">
                                                    MedicationOrder
                                                </span>
                                                <span className="circle">
                                                    2
                                                </span>
                                            </Nav.Link>
                                        </Nav.Item>

                                        {/* 8th Tab - Observation‑Laboratory */}
                                        <Nav.Item className="navigation-link">
                                            <Nav.Link eventKey="eighth" className="d-flex justify-content-between">
                                                <span className="category">
                                                    Observation‑Laboratory
                                                </span>
                                                <span className="circle">
                                                    1
                                                </span>
                                            </Nav.Link>
                                        </Nav.Item>

                                        {/* 9th Tab - Observation - Vital Signs */}
                                        <Nav.Item className="navigation-link">
                                            <Nav.Link eventKey="ninth" className="d-flex justify-content-between">
                                                <span className="category">
                                                    Observation‑Vital&nbsp;Signs
                                                </span>
                                                <span className="circle">
                                                    2
                                                </span>
                                            </Nav.Link>
                                        </Nav.Item>

                                        {/* 10th Tab - Procedure */}
                                        <Nav.Item className="navigation-link">
                                            <Nav.Link eventKey="tenth" className="d-flex justify-content-between">
                                                <span className="category">
                                                    Procedure
                                                </span>
                                                <span className="circle">
                                                    2
                                                </span>
                                            </Nav.Link>
                                        </Nav.Item>

                                        {/* 11th Tab - Patient */}
                                        <Nav.Item className="navigation-link">
                                            <Nav.Link eventKey="eleventh" className="d-flex justify-content-between">
                                                <span className="category">
                                                    Patient
                                                </span>
                                                <span className="circle">
                                                    1
                                                </span>
                                            </Nav.Link>
                                        </Nav.Item>

                                    </Nav>
                                </section>
                                {/* ENDS - All Tabs - Displayed on the left */}

                                {/* STARTS - All the Content that the Tabs Display - Displayed on the right */}
                                <section className="col-xl-9 col-lg-8 col-12">
                                    <div className="patient-content">
                                        <Tab.Content>

                                            {/* Content of Search Results */}
                                            <Tab.Pane eventKey="search">
                                                <h3 className="h5 uppercase pb-3 pt-1">2 results of Search</h3>
                                                <div className="table-responsive">
                                                    <table className="table text-white border-top">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Identifier</th>
                                                                <th scope="col">Details</th>
                                                                <th scope="col">Date</th>
                                                                <th scope="col"></th>
                                                            </tr>
                                                        </thead>        
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">ID: RES365925</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Status:</td><td className="label-details align-top">active</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Type:</td><td className="label-details align-top">allergy</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Critically:</td><td className="label-details align-top">high</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Category:</td><td className="label-details align-top">food</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Name:</td><td className="label-details align-top">peanuts</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2018/05/07</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">ID: RES365924</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Status:</td><td className="label-details align-top">active</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Type:</td><td className="label-details align-top">allergy</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Critically:</td><td className="label-details align-top">high</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Category:</td><td className="label-details align-top">medicine</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Name:</td><td className="label-details align-top">penicillin</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2018/05/07</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>                  
                                            </Tab.Pane>

                                            {/* Content of 1st Tab - AllergyIntollerance */}
                                            <Tab.Pane eventKey="first">
                                                <div className="d-flex justify-content-between mb-3 align-middle row">
                                                    <div className="col-12 col-md-8">
                                                        <h3 className="h5 mb-0 pt-1 uppercase">2 resources of type AllergyTolerance</h3>
                                                    </div>
                                                    <div className="col-12 col-md-4 btns-new-refresh d-flex justify-content-end align-items-start">
                                                    <Button variant="primary" onClick={allergyShow}>New Entry</Button>
                                                        {/* <input className="btn btn-sm bg-custom-1 btn-refresh" type="button" defaultValue="Refresh" /> */}
                                                    </div>
                                                </div>
                                                <Modal
                                                        show={one}
                                                        onHide={allergyClose}
                                                        backdrop="static"
                                                        keyboard={false}
                                                    >
                                                        <Modal.Header closeButton>
                                                        <Modal.Title>Allergy Tolerance</Modal.Title>
                                                        </Modal.Header>
                                                        <Modal.Body>
                                                        <form>
                                                        <div className="form-group">
                                                            <label htmlFor="exampleFormControlSelect1">Status</label>
                                                            <select className="form-control" id="exampleFormControlSelect1">
                                                                <option>Active</option>
                                                                <option>Not Active</option>
                                                            </select>
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Type</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1"/>
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlSelect1">Critically</label>
                                                            <select className="form-control" id="exampleFormControlSelect1">
                                                                <option>Select One</option>
                                                                <option>Immediate (type I)</option>
                                                                <option>Cytotoxic (type II)</option>
                                                                <option>Immune complex-mediated (type III)</option>
                                                                <option>Delayed hypersensitivity (type IV)</option>
                                                            </select>
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlSelect1">Category</label>
                                                            <select className="form-control" id="exampleFormControlSelect1">
                                                                <option>Select One</option>
                                                                <option>Drug Allergy</option>
                                                                <option>Food Allergy</option>
                                                                <option>Insect Allergy</option>
                                                                <option>Latex Allergy</option>
                                                                <option>Mold Allergy</option>
                                                                <option>Pet Allergy</option>
                                                                <option>Pollen Allergy</option>
                                                            </select>
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Allergy Name</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1" />
                                                            </div>
                                                        </form>
                                                        </Modal.Body>
                                                        <Modal.Footer>
                                                        <Button variant="secondary" onClick={allergyClose}>
                                                            Close
                                                        </Button>
                                                        <Button variant="primary" onClick={allergyClose}>Save</Button>
                                                        </Modal.Footer>
                                                    </Modal>
                                                
                                                <div className="table-responsive">
                                                    <table className="table text-white border-top">
                                                        <thead>
                                                            <tr>
                                                            <th scope="col">Identifier</th>
                                                            <th scope="col">Details</th>
                                                            <th scope="col">Date</th>
                                                            <th scope="col"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">ID: RES365925</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Status:</td><td className="label-details align-top">active</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Type:</td><td className="label-details align-top">allergy</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Critically:</td><td className="label-details align-top">high</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Category:</td><td className="label-details align-top">food</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Name:</td><td className="label-details align-top">peanuts</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2018/05/07</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">ID: RES365924</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Status:</td><td className="label-details align-top">active</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Type:</td><td className="label-details align-top">allergy</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Critically:</td><td className="label-details align-top">high</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Category:</td><td className="label-details align-top">medicine</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Name:</td><td className="label-details align-top">penicillin</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2018/05/07</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </Tab.Pane>

                                            {/* Content of 2nd Tab - CarePlan */}
                                            <Tab.Pane eventKey="second">
                                                <div className="d-flex justify-content-between mb-3 align-middle row">
                                                    <div className="col-12 col-md-8">
                                                        <h3 className="h5 mb-0 pt-1 uppercase">3 resources of type CarePlan</h3>
                                                    </div>
                                                    <div className="col-12 col-md-4 btns-new-refresh d-flex justify-content-end align-items-start">
                                                    <Button variant="primary" onClick={careShow}>New Entry</Button>
                                                        {/* <input className="btn btn-sm bg-custom-1 btn-refresh" type="button" defaultValue="Refresh" /> */}
                                                    </div>

                                                    <Modal
                                                        show={two}
                                                        onHide={careClose}
                                                        backdrop="static"
                                                        keyboard={false}
                                                    >
                                                        <Modal.Header closeButton>
                                                        <Modal.Title>Care Plan</Modal.Title>
                                                        </Modal.Header>
                                                        <Modal.Body>
                                                        <form>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Diagnosis</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1" />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Goals/Oucomes</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1" />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Interventions</label>
                                                            <textarea className="form-control" id="exampleFormControlTextarea1" rows={3} defaultValue={""} />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Evaluation</label>
                                                            <textarea className="form-control" id="exampleFormControlTextarea1" rows={3} defaultValue={""} />
                                                            </div>
                                                        </form>
                                                        </Modal.Body>
                                                        <Modal.Footer>
                                                        <Button variant="secondary" onClick={careClose}>
                                                            Close
                                                        </Button>
                                                        <Button variant="primary" onClick={careClose}>Save</Button>
                                                        </Modal.Footer>
                                                    </Modal>



                                                </div>
                                                <div className="table-responsive">
                                                    <table className="table text-white border-top">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Identifier</th>
                                                                <th scope="col">Details</th>
                                                                <th scope="col">Date</th>
                                                                <th scope="col"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">ID: RES365914</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Diagnosis:</td><td className="label-details align-top">Arthritis</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Goals/Oucomes:</td><td className="label-details align-top">Reduced pain</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Interventions:</td><td className="label-details align-top">Physiotherapy once per week</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Evaluation:</td><td className="label-details align-top">Decreased Pain Reported</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2019/25/08</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">ID: RES365975</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Diagnosis:</td><td className="label-details align-top">Broken Arm</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Goals/Oucomes:</td><td className="label-details align-top">Increased mobility</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Interventions:</td><td className="label-details align-top">Physiotherapy twice per week</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Evaluation:</td><td className="label-details align-top">Increased Range of Motion</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2020/03/09</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </Tab.Pane>

                                            {/* Content of 3rd Tab - Condition */}
                                            <Tab.Pane eventKey="third">
                                                <div className="d-flex justify-content-between mb-3 align-middle row">
                                                    <div className="col-12 col-md-8">
                                                        <h3 className="h5 mb-0 pt-1 uppercase">2 resources of type Condition</h3>
                                                    </div>
                                                    <div className="col-12 col-md-4 btns-new-refresh d-flex justify-content-end align-items-start">
                                                    <Button variant="primary" onClick={conditionShow}>New Entry</Button>
                                                        {/* <input className="btn btn-sm bg-custom-1 btn-refresh" type="button" defaultValue="Refresh" /> */}
                                                    </div>

                                                    <Modal
                                                        show={three}
                                                        onHide={conditionClose}
                                                        backdrop="static"
                                                        keyboard={false}
                                                    >
                                                        <Modal.Header closeButton>
                                                        <Modal.Title>Condition</Modal.Title>
                                                        </Modal.Header>
                                                        <Modal.Body>
                                                        <form>

                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Diagnosis</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1" />
                                                            </div>
                                                            
                                                        </form>
                                                        </Modal.Body>
                                                        <Modal.Footer>
                                                        <Button variant="secondary" onClick={conditionClose}>
                                                            Close
                                                        </Button>
                                                        <Button variant="primary" onClick={conditionClose}>Save</Button>
                                                        </Modal.Footer>
                                                    </Modal>

                                                </div>
                                                <div className="table-responsive">
                                                    <table className="table text-white border-top">
                                                        <thead>
                                                            <tr>
                                                            <th scope="col">Identifier</th>
                                                            <th scope="col">Details</th>
                                                            <th scope="col">Date</th>
                                                            <th scope="col"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">ID: RES365914</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Diagnosis:</td><td className="label-details align-top">Arthritis</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2010/24/08</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">ID: RES365975</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Diagnosis:</td><td className="label-details align-top">Migraines</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2017/25/09</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </Tab.Pane>

                                            {/* Content of 4th Tab - DiagnosticReport */}
                                            <Tab.Pane eventKey="fourth">
                                                <div className="d-flex justify-content-between mb-3 align-middle row">
                                                    <div className="col-12 col-md-8">
                                                        <h3 className="h5 mb-0 pt-1 uppercase">4 resources of type DiagnosticReport</h3>
                                                    </div>
                                                    <div className="col-12 col-md-4 btns-new-refresh d-flex justify-content-end align-items-start">
                                                    <Button variant="primary" onClick={diagnosticShow}>New Entry</Button>
                                                        {/* <input className="btn btn-sm bg-custom-1 btn-refresh" type="button" defaultValue="Refresh" /> */}
                                                    </div>

                                                    <Modal
                                                        show={four}
                                                        onHide={diagnosticClose}
                                                        backdrop="static"
                                                        keyboard={false}
                                                    >
                                                        <Modal.Header closeButton>
                                                        <Modal.Title>Diagnostic Report</Modal.Title>
                                                        </Modal.Header>
                                                        <Modal.Body>
                                                        <form>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Condition</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1"  />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlTextarea1">Report Type</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1"  />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlTextarea1">Report</label>
                                                            <textarea className="form-control" id="exampleFormControlTextarea1" rows={3} defaultValue={""} />
                                                            </div>
                                                        </form>
                                                        </Modal.Body>
                                                        <Modal.Footer>
                                                        <Button variant="secondary" onClick={diagnosticClose}>
                                                            Close
                                                        </Button>
                                                        <Button variant="primary" onClick={diagnosticClose}>Save</Button>
                                                        </Modal.Footer>
                                                    </Modal>

                                                </div>
                                                <div className="table-responsive">
                                                    <table className="table text-white border-top">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Identifier</th>
                                                                <th scope="col">Details</th>
                                                                <th scope="col">Date</th>
                                                                <th scope="col"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">ID: RES365925</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Condition:</td><td className="label-details align-top">Broken Arm</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Report&nbsp;Type:</td><td className="label-details align-top">X-Ray</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Report:</td><td className="label-details align-top">View</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2018/05/07</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">ID: RES365925</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Condition:</td><td className="label-details align-top">Broken Arm</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Report&nbsp;Type:</td><td className="label-details align-top">X-Ray </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Report:</td><td className="label-details align-top">View</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2018/01/04</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </Tab.Pane>

                                            {/* Content of 5th Tab - Encounter */}
                                            <Tab.Pane eventKey="fifth">
                                                <div className="d-flex justify-content-between mb-3 align-middle row">
                                                    <div className="col-12 col-md-8">
                                                        <h3 className="h5 mb-0 pt-1 uppercase">2 resources of type Encounter</h3>
                                                    </div>
                                                    <div className="col-12 col-md-4 btns-new-refresh d-flex justify-content-end align-items-start">
                                                    <Button variant="primary" onClick={encounterShow}>New Entry</Button>
                                                        {/* <input className="btn btn-sm bg-custom-1 btn-refresh" type="button" defaultValue="Refresh" /> */}
                                                    </div>

                                                    <Modal
                                                        show={five}
                                                        onHide={encounterClose}
                                                        backdrop="static"
                                                        keyboard={false}
                                                    >
                                                        <Modal.Header closeButton>
                                                        <Modal.Title>Encounter</Modal.Title>
                                                        </Modal.Header>
                                                        <Modal.Body>
                                                        <form>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Symptoms</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1"  />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlTextarea1">Onset</label>
                                                            <input className="form-control"  type="date" id="exampleFormControlTextarea1"  defaultValue={""} />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlTextarea1">Treatment</label>
                                                            <textarea className="form-control" id="exampleFormControlTextarea1" rows={3} defaultValue={""} />
                                                            </div>
                                                        </form>
                                                        </Modal.Body>
                                                        <Modal.Footer>
                                                        <Button variant="secondary" onClick={encounterClose}>
                                                            Close
                                                        </Button>
                                                        <Button variant="primary" onClick={encounterClose}>Save</Button>
                                                        </Modal.Footer>
                                                    </Modal>
                                                    
                                                </div>
                                                <div className="table-responsive">
                                                    <table className="table text-white border-top">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Identifier</th>
                                                                <th scope="col">Details</th>
                                                                <th scope="col">Date</th>
                                                                <th scope="col"></th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">ID: RES365534</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Symptoms:</td><td className="label-details align-top">Fever, runny nose</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Onset:</td><td className="label-details align-top">Jan 1, 2019</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Treatment:</td><td className="label-details align-top">Rest and Fluids</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2019/05/01</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                                <tr>
                                                                <th scope="row">ID: RES365123</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Symptoms:</td><td className="label-details align-top">Fever, cough,runny nose, earache</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Onset:</td><td className="label-details align-top">Jan 10, 2018</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Treatment:</td><td className="label-details align-top">Antibiotics, fluids, rest</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2019/05/01</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </Tab.Pane>

                                            {/* Content of 6th Tab - Immunization */}
                                            <Tab.Pane eventKey="sixth">
                                                <div className="d-flex justify-content-between mb-3 align-middle row">
                                                    <div className="col-12 col-md-8">
                                                        <h3 className="h5 mb-0 pt-1 uppercase">2 resources of type Immunization</h3>
                                                    </div>
                                                    <div className="col-12 col-md-4 btns-new-refresh d-flex justify-content-end align-items-start">
                                                    <Button variant="primary" onClick={immunizationShow}>New Entry</Button>
                                                        {/* <input className="btn btn-sm bg-custom-1 btn-refresh" type="button" defaultValue="Refresh" /> */}
                                                    </div>

                                                    <Modal
                                                        show={six}
                                                        onHide={immunizationClose}
                                                        backdrop="static"
                                                        keyboard={false}
                                                    >
                                                        <Modal.Header closeButton>
                                                        <Modal.Title>Immunization</Modal.Title>
                                                        </Modal.Header>
                                                        <Modal.Body>
                                                        <form>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Name</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1" />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlTextarea1">Dose Date</label>
                                                            <input className="form-control"  type="date" id="exampleFormControlTextarea1"  defaultValue={""} />
                                                            </div>
                                                        </form>
                                                        </Modal.Body>
                                                        <Modal.Footer>
                                                        <Button variant="secondary" onClick={immunizationClose}>
                                                            Close
                                                        </Button>
                                                        <Button variant="primary" onClick={immunizationClose}>Save</Button>
                                                        </Modal.Footer>
                                                    </Modal>
                                                </div>
                                                <div className="table-responsive">
                                                    <table className="table text-white border-top">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Identifier</th>
                                                                <th scope="col">Details</th>
                                                                <th scope="col">Date</th>
                                                                <th scope="col"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">ID: RES365925</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Name:</td><td className="label-details align-top">Hepatitis B</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Dose&nbsp;Date:</td><td className="label-details align-top">01-01-2007</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Dose&nbsp;Date:</td><td className="label-details align-top">01-02-2007</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Dose&nbsp;Date:</td><td className="label-details align-top">01-03-2007</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2018/05/07</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">ID: RES365924</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Name:</td><td className="label-details align-top">Hepatitis A</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Dose&nbsp;Date:</td><td className="label-details align-top">05-01-2010</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Dose&nbsp;Date:</td><td className="label-details align-top">05-02-2010</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Dose&nbsp;Date:</td><td className="label-details align-top">05-03-2010</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2018/05/07</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </Tab.Pane>

                                            {/* Content of 7th Tab - MedicationOrder */}
                                            <Tab.Pane eventKey="seventh">
                                                <div className="d-flex justify-content-between mb-3 align-middle row">
                                                    <div className="col-12 col-md-8">
                                                        <h3 className="h5 mb-0 pt-1 uppercase">2 resources of type MedicationOrder</h3>
                                                    </div>
                                                    <div className="col-12 col-md-4 btns-new-refresh d-flex justify-content-end align-items-start">
                                                    <Button variant="primary" onClick={medicationShow}>New Entry</Button>
                                                        {/* <input className="btn btn-sm bg-custom-1 btn-refresh" type="button" defaultValue="Refresh" /> */}
                                                    </div>

                                                    <Modal
                                                        show={seven}
                                                        onHide={medicationClose}
                                                        backdrop="static"
                                                        keyboard={false}
                                                    >
                                                        <Modal.Header closeButton>
                                                        <Modal.Title>Medication Order</Modal.Title>
                                                        </Modal.Header>
                                                        <Modal.Body>
                                                        <form>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Medication</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1"  />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Dose</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1"  />
                                                            </div>
                                                            
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlTextarea1">Instructions</label>
                                                            <textarea className="form-control" id="exampleFormControlTextarea1" rows={3} defaultValue={""} />
                                                            </div>
                                                        </form>
                                                        </Modal.Body>
                                                        <Modal.Footer>
                                                        <Button variant="secondary" onClick={medicationClose}>
                                                            Close
                                                        </Button>
                                                        <Button variant="primary" onClick={medicationClose}>Save</Button>
                                                        </Modal.Footer>
                                                    </Modal>
                                                </div>
                                                <div className="table-responsive">
                                                    <table className="table text-white border-top">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Identifier</th>
                                                                <th scope="col">Details</th>
                                                                <th scope="col">Date</th>
                                                                <th scope="col"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">ID: RES365933</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Medication:</td><td className="label-details align-top">Tylenol-Codeine NO.3</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Dose:</td><td className="label-details align-top">60mg/300mg</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Instructions:</td><td className="label-details align-top">Take with food up to 3 times a day, every 6 hours</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2021/02/02</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">ID: RES125921</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Medication:</td><td className="label-details align-top">Ibuprofen</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Dose:</td><td className="label-details align-top">400mg</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Instructions:</td><td className="label-details align-top">Prescription: 400-800 mg PO q6hr; not to exceed 3200 mg/day, with food</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2021/02/02</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </Tab.Pane>

                                            {/* Content of 8th Tab - Observation - Laboratory */}
                                            <Tab.Pane eventKey="eighth">
                                                <div className="d-flex justify-content-between mb-3 align-middle row">
                                                    <div className="col-12 col-md-8">
                                                        <h3 className="h5 mb-0 pt-1 uppercase">1 resources of type Observation - Laboratory</h3>
                                                    </div>
                                                    <div className="col-12 col-md-4 btns-new-refresh d-flex justify-content-end align-items-start">
                                                    <Button variant="primary" onClick={oLShow}>New Entry</Button>
                                                        {/* <input className="btn btn-sm bg-custom-1 btn-refresh" type="button" defaultValue="Refresh" /> */}
                                                    </div>

                                                        <Modal
                                                        show={eight}
                                                        onHide={oLClose}
                                                        backdrop="static"
                                                        keyboard={false}
                                                    >
                                                        <Modal.Header closeButton>
                                                        <Modal.Title>Observation - Laboratory</Modal.Title>
                                                        </Modal.Header>
                                                        <Modal.Body>
                                                        <form>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Name 1</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1"  />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Name 2</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1"  />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Name 3</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1"  />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Name 3</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1"  />
                                                            </div>
                                                        </form>
                                                        </Modal.Body>
                                                        <Modal.Footer>
                                                        <Button variant="secondary" onClick={oLClose}>
                                                            Close
                                                        </Button>
                                                        <Button variant="primary" onClick={oLClose}>Save</Button>
                                                        </Modal.Footer>
                                                    </Modal>
                                                </div>
                                                <div className="table-responsive">
                                                    <table className="table text-white border-top">
                                                        <thead>
                                                            <tr>
                                                            <th scope="col">Identifier</th>
                                                            <th scope="col">Details</th>
                                                            <th scope="col">Date</th>
                                                            <th scope="col"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">ID: RES367725</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Name&nbsp;1:</td><td className="label-details align-top">Iron</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Name&nbsp;2:</td><td className="label-details align-top">B12</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Name&nbsp;3:</td><td className="label-details align-top">D</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Results:</td><td className="label-details align-top">Uploaded</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2021/01/01</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </Tab.Pane>

                                            {/* Content of 9th Tab - Observation - Vital Signs */}
                                            <Tab.Pane eventKey="ninth">
                                                <div className="d-flex justify-content-between mb-3 align-middle row">
                                                    <div className="col-12 col-md-8">
                                                        <h3 className="h5 mb-0 pt-1 uppercase">2 resources of type Observation - Vital Signs</h3>
                                                    </div>
                                                    <div className="col-12 col-md-4 btns-new-refresh d-flex justify-content-end align-items-start">
                                                    <Button variant="primary" onClick={oVShow}>New Entry</Button>
                                                        {/* <input className="btn btn-sm bg-custom-1 btn-refresh" type="button" defaultValue="Refresh" /> */}
                                                    </div>

                                                    <Modal
                                                        show={nine}
                                                        onHide={oVClose}
                                                        backdrop="static"
                                                        keyboard={false}
                                                    >
                                                        <Modal.Header closeButton>
                                                        <Modal.Title>Observation - Vital Signs</Modal.Title>
                                                        </Modal.Header>
                                                        <Modal.Body>
                                                        <form>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Blood Pressure</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1"  />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Breathing</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1"  />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Pulse</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1"  />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Temperature</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1"  />
                                                            </div>
                                                        </form>
                                                        </Modal.Body>
                                                        <Modal.Footer>
                                                        <Button variant="secondary" onClick={oVClose}>
                                                            Close
                                                        </Button>
                                                        <Button variant="primary" onClick={oVClose}>Save</Button>
                                                        </Modal.Footer>
                                                    </Modal>
                                                </div>
                                                <div className="table-responsive">
                                                    <table className="table text-white border-top">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Identifier</th>
                                                                <th scope="col">Details</th>
                                                                <th scope="col">Date</th>
                                                                <th scope="col"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">ID: RES365925</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Blood&nbsp;Pressure:</td><td className="label-details align-top">90/60 mm Hg</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Breathing:</td><td className="label-details align-top">12 breaths per minute</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Pulse:</td><td className="label-details align-top">60 beats per minute</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Temperature:</td><td className="label-details align-top">97.8°F</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2018/05/07</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">ID: RES365924</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Blood&nbsp;Pressure:</td><td className="label-details align-top">120/80 mm Hg</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Breathing:</td><td className="label-details align-top">18 breaths per minute</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Pulse:</td><td className="label-details align-top">100 beats per minute</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Temperature:</td><td className="label-details align-top">99.1°F</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2018/05/07</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </Tab.Pane>

                                            {/* Content of 10th Tab - Procedure */}
                                            <Tab.Pane eventKey="tenth">
                                                <div className="d-flex justify-content-between mb-3 align-middle row">
                                                    <div className="col-12 col-md-8">
                                                        <h3 className="h5 mb-0 pt-1 uppercase">2 resources of type Procedure</h3>
                                                    </div>
                                                    <div className="col-12 col-md-4 btns-new-refresh d-flex justify-content-end align-items-start">
                                                    <Button variant="primary" onClick={procedureShow}>New Entry</Button>
                                                        {/* <input className="btn btn-sm bg-custom-1 btn-refresh" type="button" defaultValue="Refresh" /> */}
                                                    </div>

                                                    <Modal
                                                        show={ten}
                                                        onHide={procedureClose}
                                                        backdrop="static"
                                                        keyboard={false}
                                                    >
                                                        <Modal.Header closeButton>
                                                        <Modal.Title>Procedure</Modal.Title>
                                                        </Modal.Header>
                                                        <Modal.Body>
                                                        <form>

                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlInput1">Procedure</label>
                                                            <input type="text" className="form-control" id="exampleFormControlInput1"  />
                                                            </div>
                                                            <div className="form-group">
                                                            <label htmlFor="exampleFormControlSelect1">Resolved</label>
                                                            <select className="form-control" id="exampleFormControlSelect1">
                                                                <option>Select One</option>
                                                                <option>Yes</option>
                                                                <option>No</option>
                                                            </select>

                                                            </div>
                                                            
                                                        </form>
                                                        </Modal.Body>
                                                        <Modal.Footer>
                                                        <Button variant="secondary" onClick={procedureClose}>
                                                            Close
                                                        </Button>
                                                        <Button variant="primary" onClick={procedureClose}>Save</Button>
                                                        </Modal.Footer>
                                                    </Modal>
                                                </div>

                                                <div className="table-responsive">
                                                    <table className="table text-white border-top">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Identifier</th>
                                                                <th scope="col">Details</th>
                                                                <th scope="col">Date</th>
                                                                <th scope="col"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">ID: RES365925</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Procedure:</td><td className="label-details align-top">Wart Removal</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Resolved:</td><td className="label-details align-top">No</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2017/05/05</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">ID: RES365925</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Procedure:</td><td className="label-details align-top">Wart Removal</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Resolved:</td><td className="label-details align-top">Yes</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2017/08/06</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </Tab.Pane>
                                            
                                            {/* Content of 11th Tab - Patient */}
                                            <Tab.Pane eventKey="eleventh">
                                                <div className="d-flex justify-content-between mb-3 align-middle row">
                                                    <div className="col-12 col-md-8">
                                                        <h3 className="h5 mb-0 pt-1 uppercase">1 resources of type Patient</h3>
                                                    </div>
                                                    <div className="col-12 col-md-4 btns-new-refresh d-flex justify-content-end align-items-start">
                                                        {/* <input className="btn btn-sm bg-custom-1 btn-refresh" type="button" defaultValue="Refresh" /> */}
                                                    </div>

                                                </div>
                                                <div className="table-responsive">
                                                    <table className="table text-white border-top">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Identifier</th>
                                                                <th scope="col">Details</th>
                                                                <th scope="col">Date</th>
                                                                <th scope="col"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">ID: RES365925</th>
                                                                <td>
                                                                    <table>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td className="label-details align-top">Salutation:</td><td className="label-details align-top">Mr.</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">First&nbsp;Name:</td><td className="label-details align-top">Norman</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Last&nbsp;Name:</td><td className="label-details align-top">Graham</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Gender:</td><td className="label-details align-top">Male</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">DOB:</td><td className="label-details align-top">1953-09-18</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Address:</td><td className="label-details align-top">1681 Mosciski Road 02205 Boston MA US</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Phone:</td><td className="label-details align-top">(588) 082-5762 x312</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">Email:</td><td className="label-details align-top">unknown</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">ID:</td><td className="label-details align-top">0b1242d9-fd7e-4dbf-8e2d-c8428557b720</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td className="label-details align-top">MRN:</td><td className="label-details align-top">27fc5e5c-f34b-48b1-b74d-a3408ce9c4ce</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td>Last Updated:<br /> 2018/05/07</td>
                                                                <td> 
                                                                    <div className="btns-new-refresh d-flex justify-content-start align-items-start">
                                                                        
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </Tab.Pane>

                                        </Tab.Content>
                                    </div>
                                </section>
                                {/* ENDS - All the Content that the Tabs Display - Displayed on the right */}

                            </Row>
                        </Tab.Container>
                    </section>
                    {/* ENDS - All Patient Medical Data - Includes Left-Side Tabs and Right-Side Data Displayed */}
                
                </main>
            </div>

        </>
    )
}

export default Summary;