import React, { Component } from "react";
import { singlePatient, remove } from "./apiPatient";
import DefaultPatient from "../images/mountains.jpg";
import { Link, Redirect } from "react-router-dom";
import { isAuthenticated } from "../auth";
import NewRecord from '../record/NewRecord';
import Records from '../record/Records';

class SinglePatient extends Component {
	state = {
		patient: "",
		commentId: '',
		redirectToHome: false,
		redirectToSignin: false,
		test: null
	};


	componentDidMount = () => {
        const patientId = this.props.match.params.patientId;

		singlePatient(patientId).then((data) => {
			if (data.error) {
				console.log(data.error);
			} else {
				this.setState({
				// 	comments: dataComments,
				patient: data
				});
			}
		});
	};



	deletePatient = () => {
		const patientId = this.props.match.params.patientId;
		const token = isAuthenticated().token;
		remove(patientId, token).then((data) => {
			if (data.error) {
				console.log(data.error);
			} else {
				this.setState({ redirectToHome: true });
			}
		});
	};

	deleteConfirmed = () => {
		let answer = window.confirm("Are you sure you want to delete your patient?");
		if (answer) {
			this.deletePatient();
		}
	};


	renderPatient = (patient) => {
		const posterId = patient.postedBy ? `/user/${patient.postedBy}` : "";

		const posterName = patient.postedBy ? patient.fName : " Unknown";
		return (
			<div className="card-body view-patient mt-0 pt-0">
				<h2 className="pb-4">View Patient {patient.lName} </h2>
				<img
					// src={`${process.env.REACT_APP_API_URL}/patient/photo/${patient._id}`}
					src={`${DefaultPatient}`}
					alt={patient.lName}
					// onError={(i) => (i.target.src = `${Defaultpatient}`)}
					className="img-thunbnail mb-3"
					style={{
						height: "300px",
						width: "100%",
						objectFit: "cover",
					}}
				/>

				<p className="card-text">{patient.information}</p>
				<br />
				<p className="font-italic mark">
					Posted by <Link to={`${posterId}`}>{posterName} </Link>
					on {new Date(patient.created).toDateString()}
				</p>
				<div className="d-inline-block">
					<Link
						to={`/findpatients`}
						className="btn btn-raised btn-primary mr-4 mb-3"
					>
						Back to Patients
					</Link>

					{isAuthenticated().user &&
						isAuthenticated().user._id == patient.postedBy && (
							<>
								<Link
									to={`/patient/edit/${patient.patient_id }`}
									className="btn btn-raised btn-info mr-4 mb-3 text-dark"
								>
									Update patient
								</Link>
								<button
									onClick={this.deleteConfirmed}
									className="btn btn-raised btn-danger mb-3"
								>
									Delete patient
								</button>
							</>
						)}

					<div>
						{isAuthenticated().user && isAuthenticated().user.role === "admin" && (
							<div class="card mt-5">
								<div className="card-body">
									<h5 className="card-title">Admin</h5>
									<p className="mb-2 text-danger">Edit/Delete as an Admin</p>
									<Link
										to={`/patient/edit/${patient.patient_id}`}
										className="btn btn-raised btn-warning mb-3 mr-4 text-white"
									>
										Update patient
									</Link>
									<button
										onClick={this.deleteConfirmed}
										className="btn btn-raised btn-danger mb-3"
									>
										Delete patient
									</button>
								</div>
							</div>
						)}
					</div>
				</div>
			</div>
		);
	};



	render() {
		const { patient, redirectToHome, redirectToSignin } = this.state;

		if (redirectToHome) {
			return <Redirect to={`/`} />;
		} else if (redirectToSignin) {
			return <Redirect to={`/signin`} />;
		}
		return (
			<div className="container">
				<h2 className="display-2 mt-5 mb-5">{patient.name}</h2>

				{!patient ? (
					<div className="jumbotron text-center">
						<h2>Loading...</h2>
					</div>
				) : (
					this.renderPatient(patient)
				)}

				<NewRecord patient_id={patient.patient_id} parentCallback={this.handleCallback} />
				<Records patient_id={this.props.match.params.patientId}/>
			</div>
		);
	}
}

export default SinglePatient;
