import React, { Component } from "react";
import Records from "../patient/Patients";
import Users from "../user/Users";
import { isAuthenticated } from "../auth";
import { Redirect } from "react-router-dom";
import './../App.css';

class Admin extends Component {
    state = {
        redirectToHome: false
    };

    componentDidMount() {
        if (isAuthenticated().user.role !== "admin") {
            this.setState({ redirectToHome: true });
        }
    }

    render() {
        if (this.state.redirectToHome) {
            return <Redirect to="/" />;
        }

        return (
            <div className="admin-dashboard">
                <div className="jumbotron mb-0 admin-jumbo">
                    <h1 className="text-dark text-center title">Admin Dashboard</h1>
                    <p className="h4 text-dark text-center title">ERM Talens General Hospital</p>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-6">
                            <h2 className="posts">Posts</h2>
                            {/* <hr /> */}
                            <Records />
                        </div>
                        <div className="col-md-6">
                            <h2 className="posts">Users</h2>
                            {/* <hr /> */}
                            <Users />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Admin;
