import React, { Component } from "react";
import { singleRecord, remove } from "./apiRecord";
import DefaultPatient from "../images/mountains.jpg";
import { Link, Redirect } from "react-router-dom";
import { isAuthenticated } from "../auth";

class SingleRecord extends Component {
	state = {
		record: "",
		redirectToHome: false,
		redirectToSignin: false,
		test: null,
	};

    componentDidMount = () => {
        
        const recordId = this.props.match.params.recordId;

		singleRecord(recordId).then((data) => {
			console.log(data);
			if (data.error) {
				console.log(data.error);
			} else {
				this.setState({
					record: data,
				});
			}
		});
	};

	deleteRecord = () => {
		const recordId = this.props.match.params.recordId;
		const token = isAuthenticated().token;
		remove(recordId, token).then((data) => {
			if (data.error) {
				console.log(data.error);
			} else {
				this.setState({ redirectToHome: true });
			}
		});
	};

	deleteConfirmed = () => {
		let answer = window.confirm("Are you sure you want to delete your record?");
		if (answer) {
			this.deleteRecord();
		}
	};


	renderRecord = (record) => {

		const posterId = record.postedBy ? `/user/${record.postedBy}` : "";

		const posterName = record.postedBy ? record.fName : " Unknown";
		return (
			<div className="card-body mt-0 pt-0">
				<h2 className="patient-record pb-5 pt-1">Patient Record</h2>
				<img
					// src={`${process.env.REACT_APP_API_URL}/patient/photo/${patient._id}`}
					src={`${DefaultPatient}`}
					alt={record.patient_lastName}
					// onError={(i) => (i.target.src = `${Defaultpatient}`)}
					className="img-thunbnail mb-3"
					style={{
						height: "300px",
						width: "100%",
						objectFit: "cover",
					}}
				/>
				<h1 className="text-white">Record title: {record.title}</h1>
				<h5 className="text-white">Record body: {record.body}</h5>
				<br />
				<p className="font-italic mark">
					Posted by <Link to={`${posterId}`}>{posterName} </Link>
					on {new Date(record.created).toDateString()}
				</p>
				<div className="d-inline-block">
					<Link
						to={`/findrecords`}
						className="btn btn-raised btn-primary btn-sm mr-5"
					>
						Back to records
					</Link>

					{isAuthenticated().user &&
						isAuthenticated().user._id == record.postedBy && (
							<>
								<Link
									to={`/record/edit/${record.record_id}`}
									className="btn btn-raised btn-warning btn-sm mr-5"
								>
									Update record
								</Link>
								<button
									onClick={this.deleteConfirmed}
									className="btn btn-raised btn-danger"
								>
									Delete record
								</button>
							</>
						)}

					<div>
						{isAuthenticated().user && isAuthenticated().user.role === "admin" && (
							<div class="card mt-5">
								<div className="card-body">
									<h5 className="card-title">Admin</h5>
									<p className="mb-2 text-danger">Edit/Delete as an Admin</p>
									<Link
										to={`/record/edit/${record._id}`}
										className="btn btn-raised btn-warning btn-sm mr-5"
									>
										Update record
									</Link>
									<button
										onClick={this.deleteConfirmed}
										className="btn btn-raised btn-danger"
									>
										Delete record
									</button>
								</div>
							</div>
						)}
					</div>
				</div>
			</div>
		);
	};

	render() {
		const { record, redirectToHome, redirectToSignin } = this.state;

		if (redirectToHome) {
			return <Redirect to={`/`} />;
		} else if (redirectToSignin) {
			return <Redirect to={`/signin`} />;
		}

        return (
            
			<div className="container">
				<h2 className="display-2 mt-5 mb-5">{record.patient_lastName}</h2>

				{!record ? (
					<div className="jumbotron text-center">
						<h2>Loading...</h2>
					</div>
				) : (
					this.renderRecord(record)
                    
				)}
			</div>
			// <div className="create-medical-personelle p-5">
			// 	<p>Create New Medical Personelle Account</p>
			// </div>
		);
	}
}

export default SingleRecord;
