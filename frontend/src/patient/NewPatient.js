import React, { Component } from "react";
import { isAuthenticated } from "../auth";
import { create } from "./apiPatient";
    import { Redirect } from "react-router-dom";

class NewPatient extends Component {
    constructor() {
        super();
        this.state = {
            name: "",
            fName: "", //first name
            lName: "",  //last name
            gender: "",
            dob: "",
            address: "",
            pNumber: "",
            email: "",
            hCard: "",
            information: "",
            photo: "",
            error: "",
            user: {},
            fileSize: 0,
            loading: false,
            redirectPatient: false
        };
    }

    componentDidMount() {
        this.formData = new FormData();
        this.setState({ user: isAuthenticated().user });
    }

    isValid = () => {
        const { fName, lName, gender, dob,  address, pNumber, email, hCard, information, fileSize } = this.state; //added lname
        if (fileSize > 100000) {
            this.setState({
                error: "File size should be less than 100kb",
                loading: false
            });
            return false;
        }

        //lname added below
        if (fName.length === 0 || lName.length === 0 || gender.length === 0 || dob.length === 0 || address.length === 0 || pNumber.length === 0 || email.length === 0 || hCard.length === 0 ||information.length === 0) {
            this.setState({ error: "All fields are required", loading: false });
            return false;
        }
        return true;
    };

    handleChange = name => event => {
        this.setState({ error: "" });
        const value =
            name === "photo" ? event.target.files[0] : event.target.value;

        const fileSize = name === "photo" ? event.target.files[0].size : 0;
        this.formData.set(name, value);
        this.setState({ [name]: value, fileSize });
    };

    clickSubmit = event => {
        event.preventDefault();
        this.setState({ loading: true });

        if (this.isValid()) {
            const userId = isAuthenticated().user._id;
            const token = isAuthenticated().token;


            create(userId, token, this.formData).then(data => {
                if (data.error) this.setState({ error: data.error });
                else {
                    this.setState({
                        loading: false,
                        fName: "",
                        lName: "",
                        gender: "",
                        dob: "",
                        address: "",
                        pNumber: "",
                        email: "",
                        hCard: "",
                        information: "",
                        redirectToPatient: true
                    });
                }
            });
        }
    };

    newPatientForm = (fName, lName, gender, dob,  address, pNumber, email, hCard, information) => (
        <form>
            <div className="form-group">
                <label className="text-muted">Post Patient Photo</label>
                <input
                    onChange={this.handleChange("photo")}
                    type="file"
                    accept="image/*"
                    className="form-control"
                />
            </div>
            <div className="form-group">
                <label className="text-muted">First Name</label>
                <input
                    onChange={this.handleChange("fName")}
                    type="text"
                    className="form-control"
                    value={fName}
                />
            </div>

            <div className="form-group">
                <label className="text-muted">Last Name</label>
                <input
                    onChange={this.handleChange("lName")}
                    type="text"
                    className="form-control"
                    value={lName}
                />
            </div>

            <div className="form-group">
                <label className="text-muted">Gender</label>

                <select class="form-select" aria-label="Default select example" onChange={this.handleChange("gender")} className="form-control"
                    value={gender}>
                <option selected>Select Gender</option>
                <option value="1">Male</option>
                <option value="2">Female</option>
                <option value="3">Other</option>
              </select>
               
            </div>

            

            <div className="form-group">
                <label className="text-muted">Date Of Birth</label>
                <input
                    onChange={this.handleChange("dob")}
                    type="date"
                    className="form-control"
                    value={dob}
                />
            </div>
            

            

            <div className="form-group">
                <label className="text-muted">Address</label>
                <input
                    onChange={this.handleChange("address")}
                    type="address"
                    className="form-control"
                    value={address}
                />
            </div>

            <div className="form-group">
                <label className="text-muted">Phone Number</label>
                <input
                    onChange={this.handleChange("pNumber")}
                    type="tel"
                    className="form-control"
                    value={pNumber}
                />
            </div>

            <div className="form-group">
                <label className="text-muted">Email</label>
                <input
                    onChange={this.handleChange("email")}
                    type="email"
                    className="form-control"
                    value={email}
                />
            </div>

            <div className="form-group">
                <label className="text-muted">Health Card Number</label>
                <input
                    onChange={this.handleChange("hCard")}
                    type="text"
                    className="form-control"
                    maxLength="12"
                    value={hCard}
                />
            </div>


            <div className="form-group">
                <label className="text-muted">Information</label>
                <textarea
                    onChange={this.handleChange("information")}
                    type="text"
                    className="form-control"
                    value={information}
                />
            </div>

            <button
                onClick={this.clickSubmit}
                className="btn btn-raised btn-primary"
            >
                Create Patient
            </button>
        </form>
    );

    render() {
        const {
            fName,
            lName,
            gender,
            dob,
            address,
            pNumber,
            email,
            hCard,
            information,
            photo,
            user,
            error,
            loading,
            redirectToPatient
        } = this.state;

        if (redirectToPatient) {
            return <Redirect to={`/findpatients`} />;
        }

        return (
            <div className="container create-patient">
                <h2 className="mt-5 mb-5">Create a new patient</h2>
                <div
                    className="alert alert-danger"
                    style={{ display: error ? "" : "none" }}
                >
                    {error}
                </div>

                {loading ? (
                    <div className="jumbotron text-center">
                        <h2>Loading...</h2>
                    </div>
                ) : (
                    ""
                )}

                {this.newPatientForm(fName, lName, gender, dob, address, pNumber, email, hCard,  information)}
            </div>
        );
    }
}

export default NewPatient;
