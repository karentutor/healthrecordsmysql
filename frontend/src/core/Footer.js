import React from 'react';

const Footer = () => {
    return(
        <footer className="text-center bg-custom-2 p-3 summary">
            <p className="p-0 m-0"><small>&copy; Copyright 2021 Talens General Hospital</small></p>
        </footer>
    )
}

export default Footer;