import React, { Component } from "react";
import { list, listByUser } from "./apiPatient";
import { Link } from "react-router-dom";
import { isAuthenticated } from "../auth";
import SearchHealthCardFindPatientPg from './../summary/components/SearchHealthCardFindPatientPg';

class FindPatients extends Component {
    constructor() {
        super();
        this.state = {
            patients: [],
            error: "",
            open: false
            
        };
    }

    componentDidMount() {
        const userId = isAuthenticated().user._id;
        const token = isAuthenticated().token;
        const role = isAuthenticated().user.role;

        if (role === 'admin') {
            
            list().then(data => {

                if (data.error) {
                    console.log(data.error);
                } else {
                    this.setState({ patients: data });
                }
            });
        } else {
            listByUser(userId, token).then(data => {
                console.log('data', data);
                if (data.error) {
                    console.log(data.error);
                } else {
                    this.setState({ patients: data });
                }
            });
        }
    }

    renderPatients = patients => {

        let role = isAuthenticated().user.role;
       
        return (
            <div className="find-patient pb-5">
                <div className="row">
                    {patients.map((patient, i) => {
                        console.log(patient);

                        let patientDiv = role === 'subscriber' ? patient.patient_id : patient.hCard;
                        return (
                            <div className="card col-md-4 text-center" key={i}>
                                <div className="card-body">
                                    <h5 className="card-title">{patient.lName}</h5>
                                
                                    <div>Patient healthcard number: {patientDiv} </div>
                                    <hr />
                                    <Link
                                        to={`/patient/${patientDiv}`}
                                        className="btn btn-raised btn-primary btn-sm"
                                    >
                                        View Patient
                                </Link>

                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        );
    }

    render() {
        const { patients } = this.state;
        return (
            <div className="container find-patient">
                <h2 className="mt-5 mb-5">Find Patient</h2>
                <SearchHealthCardFindPatientPg />

                {this.renderPatients(patients)}
            </div>
        );
    }

}

export default FindPatients;