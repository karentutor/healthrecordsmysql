const formidable = require("formidable");
const fs = require("fs");
const _ = require("lodash");
const {
	createKeyString,
	createSetQuery,
	createValString,
} = require("../helpers");
const { patientQuery, patientQueryJoin } = require("../helpers/queries");

exports.createPatient = (req, res, next) => {
	let form = new formidable.IncomingForm();
	form.keepExtensions = true;
	form.parse(req, (err, fields, files) => {
		console.log(err);
		if (err) {
			return res.status(400).json({
				error: "Image could not be uploaded",
			});
		}

		const keyQuery = createKeyString(fields);
		const valQuery = createValString(fields);
		const postedBy = req.profile._id;
		const role = req.profile.role;

		if (files.photo) {
			//            patient.photo.data = fs.readFileSync(files.photo.path);
			//           patient.photo.contentType = files.photo.type;
		}
		let query = `INSERT INTO patients (${keyQuery},postedBy,role) VALUES (${valQuery}, ${postedBy}, '${role}')`;
		
		db.query(query, (err, data) => {
			if (err) {
				return res.status(500).send(err);
			}
			res.status(200).send(data);
		});
	});
};

exports.deletePatient = (req, res) => {
	let _id = req.patient.patient_id;
	let query = `DELETE FROM patients WHERE hCard=${_id}`;

	db.query(query, (err, data) => {
		if (err) {
			return res.status(500).send(err);
		}
		return res.status(200).json({
			message: "success",
		});
	});
};

exports.getPatients = async (req, res) => {
	let query = "SELECT * FROM `patients` ";

	db.query(query, (err, data) => {
		if (err) {
			return res.status(500).send(err);
		}
		res.status(200).send(data);
	});
};

exports.isPoster = (req, res, next) => {
	let sameUser =
		req.patient && req.auth && req.patient.postedBy == req.auth._id;
	let adminUser = req.patient && req.auth && req.auth.role === "admin";
	let isPoster = sameUser || adminUser;

	if (!isPoster) {
		return res.status(403).json({
			error: "User is not authorized",
		});
	}
	next();
};

exports.photo = (req, res, next) => {
	res.set("Content-Type", req.patient.photo.contentType);
	//return res.status(404);
	return send(req.patient.photo.data);
};

exports.patientsByUser = (req, res) => {
	//	let postedBy = req.profile._id;
	//	console.log('called');
	let postedBy = req.profile._id;
	let query =
		`SELECT ${patientQueryJoin}, u.fName FROM patients as p INNER JOIN users as u on p.postedBy = u._id WHERE p.postedBy = '` +
		postedBy +
		"'";
	db.query(query, (err, data) => {
		if (err) {
			return res.status(500).send(err);
		}
		res.status(200).json(data);
	});
};
exports.patientById = (req, res, next, id) => {
	let query =
		`SELECT ${patientQueryJoin}, u.fName as user_name FROM patients as p INNER JOIN users as u ON p.postedBy = u._id WHERE p.hCard =` +
		id;

	db.query(query, (err, result) => {
		if (err) {
			return res.status(500).send(err);
		} else {
			let data = JSON.parse(JSON.stringify(result[0]));
			req.patient = data; // adds profile object in req with user info
			next();
		}
	});
};

exports.singlePatient = (req, res) => {
	return res.json(req.patient);
};

exports.updatePatient = (req, res, next) => {
	let form = new formidable.IncomingForm();
	form.keepExtensions = true;
	form.parse(req, (err, fields, files) => {
		if (err) {
			return res.status(400).json({
				error: "Photo could not be uploaded",
			});
		}
		// update patient with corrected fields
		const newHCard = fields.hCard;
		let patient = req.patient;

		//update patient data with new information -- note only new information 'changed on form frone end' will be updated
		patient = _.extend(patient, fields);
		patient.updated = Date.now();
		const oldHCard = req.patient.patient_id;

        //remove uneeded items of object using lodash
        let obj = {...patient};
        obj = _.omit(obj, ['user_name', 'updated', 'patient_id']);
		obj.hCard = newHCard;
		//add and delete extraneous data for creating the sql query
		
		const setQuery = createSetQuery(obj);
		if (files.photo) {
			//	patient.photo.data = fs.readFileSync(files.photo.path);
			//	patient.photo.contentType = files.photo.type;
		}

		//let query = `UPDATE patients SET fName='${fName}', lName='${lName}',gender='${gender}',dob='${dob}',address='${address}',pNumber='${pNumber}',email='${email}',information='${information}', postedBy=${postedBy} WHERE hCard=${patient_id}`;
		let query = `UPDATE patients SET ${setQuery} WHERE hCard=${oldHCard}`;
		let query1 = `SELECT * FROM patients WHERE hCard=${newHCard}`;

		db.query(query, (err, data) => {
			if (err) {
				return res.status(500).send(err);
			} else {
				db.query(query1, (err, data) => {
					if (err) {
						return res.status(500).send(err);
					} else {
						return res.status(200).send(JSON.stringify(data[0]));
					}
				});
			}
		});
	}); //end form parse
};
