import React from "react";
import logo from "./../images/hospitalLogo.png"

const Home = () => (
  <div class="home pt-1">
    <div className="bg-custom-2 border-bottom-custom pb-5 pt-5 text-center">
      <div>
          <img className="logo" src={logo} width={150} alt="Talens General Hospital Logo" />
      </div>
      <div className="pl-2 d-inline-block">
          <h1 className="text-white p-top-custom">Talens General Hospital</h1>
          <h2 className="heading text-white text-center pt-2">Welcome to Patient Hospital Records</h2>
      </div>
    </div>
  </div>
);

export default Home;
