import React, { useEffect, useState } from 'react';
import './../Styles.css'; 
import { withRouter } from "react-router";

const OnePatientInfo = (props) => {
    console.log(props);
    let hCard = props.match.params.hCard;
    const [patientInfo, setPatientInfo] = useState([]);
    useEffect(() => {
        async function fetchData() {
        const res = await fetch(`http://localhost:8000/api/patientsummary/profile/${hCard}`);
        res
            .json()
            .then((res) => setPatientInfo(res))
        }
        fetchData();
    }, [hCard]);     

    return(
        <>

            {/* Patient Information Section at Top of Page */}
            <section className="patient-profile">

                {patientInfo.length === 0 &&
                    <h2 className="h3 bg-custom-4 patient-name"><i>No listings found - Enter Health Card Number</i></h2>
                }

                {patientInfo.length > 0 && 
                    <h2 className="h3 bg-custom-4 patient-name">{patientInfo[0].fName} {patientInfo[0].lName}</h2>
                } 

                <div className="row text-white">

                    {/* Left Column of Patient Basic Info */}
                    <div className="col-lg-4">
                        <table>
                            <tbody>
                                <tr>

                                    <td className="align-top label-patient pr-4">Gender:&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    
                                    {patientInfo.length === 0 &&
                                        <tr><td colSpan="12" className="text-center"><i>No listings found</i></td></tr>
                                    }

                                    {patientInfo.length > 0 && 
                                        <td>{patientInfo[0].gender}</td>
                                    }
                                    
                                </tr>
                                <tr>

                                    <td className="align-top label-patient pr-4">DOB:</td>
                                    
                                    {patientInfo.length === 0 &&
                                        <tr><td colSpan="12" className="text-center"><i>No listings found</i></td></tr>
                                    }

                                    {patientInfo.length > 0 && 
                                        <td>{patientInfo[0].dob}</td>
                                    }

                                </tr>
                                <tr>

                                    <td className="align-top label-patient pr-4">Age:</td>
                                    
                                    {patientInfo.length === 0 &&
                                        <tr><td colSpan="12" className="text-center"><i>No listings found</i></td></tr>
                                    }

                                    {patientInfo.length > 0 &&
                                        <td>{patientInfo[0].age}</td>
                                    }

                                </tr>
                            </tbody>
                        </table>
                    </div>

                    {/* Middle Column of Patient Basic Info */}
                    <div className="col-lg-4">
                        <table>
                            <tbody>
                                <tr>
                                    
                                    <td className="align-top label-patient pr-4">Address:&nbsp;&nbsp;</td>

                                    {patientInfo.length === 0 &&
                                        <tr><td colSpan="12" className="text-center"><i>No listings found</i></td></tr>
                                    }

                                    {patientInfo.length > 0 && 
                                        <td>{patientInfo[0].address}</td>
                                    }  

                                </tr>
                                <tr>
                                    <td className="align-top label-patient pr-4">Phone:</td>
                                    
                                    {patientInfo.length === 0 &&
                                        <tr><td colSpan="12" className="text-center"><i>No listings found</i></td></tr>
                                    }

                                    {patientInfo.length > 0 && 
                                        <td>{patientInfo[0].pNumber}</td>
                                    }

                                </tr>
                                <tr>
                                    <td className="align-top label-patient pr-4">Email:</td>
                                    
                                    {patientInfo.length === 0 &&
                                        <tr><td colSpan="12" className="text-center"><i>No listings found</i></td></tr>
                                    }

                                    {patientInfo.length > 0 && 
                                        <td>{patientInfo[0].email}</td>
                                    }

                                </tr>
                            </tbody>
                        </table>
                    </div>

                    {/* Right Column of Patient Basic Info */}
                    <div className="col-lg-4">
                        <table>
                            <tbody>
                                <tr>
                                    <td className="align-top label-patient pr-2">Health&nbsp;Card:</td>
                                    
                                    {patientInfo.length === 0 &&
                                        <tr><td colSpan="12" className="text-center"><i>No listings found</i></td></tr>
                                    }

                                    {patientInfo.length > 0 && 
                                        <td>{patientInfo[0].hCard}</td>
                                    }

                                </tr>
                                <tr>

                                    <td className="align-top label-patient pr-1">Information:</td>
                                    
                                    {patientInfo.length === 0 &&
                                        <tr><td colSpan="12" className="text-center"><i>No listings found</i></td></tr>
                                    }

                                    {patientInfo.length > 0 &&
                                        <td>{patientInfo[0].information}</td>
                                    }

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            
            </section>

        </>

    )
}

export default withRouter(OnePatientInfo);