import React, { Component } from "react";
import { singlePatient, update } from "./apiPatient";
import { isAuthenticated } from "../auth";
import { Link, Redirect } from "react-router-dom";
import DefaultPatients from "../images/mountains.jpg";

class EditPatient extends Component {
	constructor() {
		super();
		this.state = {
			id: "",
			patientId: "",
			message: "",
			fName: "", //first name
			lName: "", //last name
			gender: "",
			dob: "",
			address: "",
			pNumber: "",
			email: "",
			hCard: "",
			information: "",
			redirectToPatients: false,
			error: "",
			fileSize: 0,
			loading: false,
		};
	}

	init = (patientId) => {
		singlePatient(patientId).then((data) => {
			if (data.error) {
				this.setState({ redirectToPatients: true });
			} else {
				this.setState({
					id: data.postedBy,
					fName: data.fName,
					lName: data.lName,
					gender: data.gender,
					dob: data.dob,
					address: data.address,
					pNumber: data.pNumber,
					email: data.email,
					hCard: data.patient_id,
					information: data.information,
					patientId: patientId,
					error: "",
				});

			}
		});
	};

	componentDidMount() {
		this.patientData = new FormData();
		const patientId = this.props.match.params.patientId;
		this.init(patientId);
	}

	isValid = () => {
		// const { name, information, fileSize } = this.state;
		// if (fileSize > 1000000) {
		// 	this.setState({
		// 		error: "File size should be less than 100kb",
		// 		loading: false,
		// 	});
		// 	return false;
		// }
		// if (name.length === 0 || information.length === 0) {
		// 	this.setState({ error: "All fields are required", loading: false });
		// 	return false;
		// }
		return true;
	};

	handleChange = (name) => (event) => {
		this.setState({ error: "" });
		const value = name === "photo" ? event.target.files[0] : event.target.value;

		const fileSize = name === "photo" ? event.target.files[0].size : 0;
		this.patientData.set(name, value);
		this.setState({ [name]: value, fileSize });
	};

	clickSubmit = (event) => {
		event.preventDefault();
		this.setState({ loading: true });

		if (this.isValid()) {
			const patientId = this.props.match.params.patientId;
			const token = isAuthenticated().token;

			update(patientId, token, this.patientData).then((data) => {
				if (data.error) this.setState({ error: data.error });
				else {
					this.setState({
						loading: false,
						message: "Record updated",
						fName: data.fName,
						lName: data.lName,
						gender: data.gender,
						dob: data.dob,
						address: data.address,
						pNumber: data.pNumber,
						email: data.email,
						hCard: data.hCard,
						information: data.information,
						redirectTopatients: true,
					});
				}
			});
		}
	};

	editPatientForm = (
		fName,
		lName,
		gender,
		dob,
		address,
		pNumber,
		email,
		hCard,
		information
	) => (
		<>
			<form>
				<div className="form-group">
					<label className="text-muted">Post Patient Photo</label>
					<input
						onChange={this.handleChange("photo")}
						type="file"
						accept="image/*"
						className="form-control"
					/>
				</div>
				<div className="form-group">
					<label className="text-muted">First Name</label>
					<input
						onChange={this.handleChange("fName")}
						type="text"
						className="form-control"
						value={fName}
					/>
				</div>

				<div className="form-group">
					<label className="text-muted">Last Name</label>
					<input
						onChange={this.handleChange("lName")}
						type="text"
						className="form-control"
						value={lName}
					/>
				</div>

				<div className="form-group">
					<label className="text-muted">Gender</label>

					<select
						class="form-select"
						aria-label="Default select example"
						onChange={this.handleChange("gender")}
						className="form-control"
						value={gender}
					>
						<option selected>Select Gender</option>
						<option value="1">Male</option>
						<option value="2">Female</option>
						<option value="3">Other</option>
					</select>
				</div>

				<div className="form-group">
					<label className="text-muted">Date Of Birth</label>
					<input
						onChange={this.handleChange("dob")}
						type="date"
						className="form-control"
						value={dob}
					/>
				</div>

				<div className="form-group">
					<label className="text-muted">Address</label>
					<input
						onChange={this.handleChange("address")}
						type="address"
						className="form-control"
						value={address}
					/>
				</div>

				<div className="form-group">
					<label className="text-muted">Phone Number</label>
					<input
						onChange={this.handleChange("pNumber")}
						type="tel"
						className="form-control"
						value={pNumber}
					/>
				</div>

				<div className="form-group">
					<label className="text-muted">Email</label>
					<input
						onChange={this.handleChange("email")}
						type="email"
						className="form-control"
						value={email}
					/>
				</div>

				<div className="form-group">
					<label className="text-muted">Health Card Number</label>
					<input
						onChange={this.handleChange("hCard")}
						type="text"
						className="form-control"
						maxLength="12"
						value={hCard}
					/>
				</div>

				<div className="form-group">
					<label className="text-muted">Information</label>
					<textarea
						onChange={this.handleChange("information")}
						type="text"
						className="form-control"
						value={information}
					/>
				</div>

				<button
					onClick={this.clickSubmit}
					className="btn btn-raised btn-primary text-dark"
				>
					Update patient
				</button>
			</form>
		</>
	);

	render() {
		const {
			fName,
			lName,
			gender,
			dob,
			address,
			pNumber,
			email,
			hCard,
			information,
			photo,
			user,
			id,
			message,
			error,
			redirectToPatients,
			loading,
		} = this.state;

		if (redirectToPatients) {
			return <Redirect to={`/findpatients`} />;
		}

		return (
			<div className="container update-patient">
				<h2 className="text-white text-center pt-5">Update Patient</h2>

				<h3 className="mt-4 mb-4 text-white">{fName}</h3>

				<div
					className="alert alert-danger"
					style={{ display: error ? "" : "none" }}
				>
					{error}
				</div>

				<div
					className="alert alert-success"
					style={{ display: message ? "" : "none" }}
				>
					{message} <Link to="/findpatients"> Return to patients</Link>.
				</div>

				{loading ? (
					<div className="jumbotron text-center">
						<h2>Loading...</h2>
					</div>
				) : (
					""
				)}

				<img
					style={{ height: "200px", width: "auto" }}
					className="img-thumbnail"
					// src={`${DefaultPatients}`}
					src={`${DefaultPatients}`}
					// src={`${
					//     process.env.REACT_APP_API_URL
					// }/patient/photo/${patientId}`}
					// onError={i => (i.target.src = `${DefaultPatients}`)}
					alt={fName}
				/>

				{/* {isAuthenticated().user.role === "admin" &&
					this.editPatientForm(
						fName,
						lName,
						gender,
						dob,
						address,
						pNumber,
						email,
						hCard,
						information
					)} */}

				{/* {isAuthenticated().user._id == id &&
					this.editPatientForm(
						fName,
						lName,
						gender,
						dob,
						address,
						pNumber,
						email,
						hCard,
						information
					)} */}

				{(isAuthenticated().user._id == id || isAuthenticated().user.role === "admin") &&
					this.editPatientForm(
						fName,
						lName,
						gender,
						dob,
						address,
						pNumber,
						email,
						hCard,
						information
					)}
			</div>
		);
	}
}

export default EditPatient;
