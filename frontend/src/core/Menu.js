
import React, { Fragment } from "react";
// import { Link, withRouter } from "react-router-dom";
// ADDED NavLink with exact and activeClassName="active-link" below
import { NavLink, withRouter } from "react-router-dom";
import { signout, isAuthenticated } from "../auth";
import './../App.css';
import {Nav, Navbar} from 'react-bootstrap';
import logo from './../images/hospitalLogo.png';

const isActive = (history, path) => {
	if (history.location.pathname === path) return { color: "#ff9900" };
	else return { color: "#ffffff" };
};

const Menu = ({ history }) => {


	const user = isAuthenticated().user;
	console.log('user', user);
		return (<>

			<Navbar collapseOnSelect expand="lg" className="bg-custom-4 py-0 padding-custom" variant="dark">
				<Navbar.Brand href="/"><img className="logo" src={logo} width={50} height={50} alt="Talens General Hospital Logo" /></Navbar.Brand>
				<Navbar.Toggle aria-controls="responsive-navbar-nav" />
				<Navbar.Collapse id="responsive-navbar-nav">
					<Nav className="mr-auto">
						<li className="nav-item">
							<NavLink exact activeClassName="active-link" className="nav-link" style={isActive(history, "/")} to="/">
								Home
						</NavLink>
						</li>

						{
							!user && (
								<React.Fragment>
									<li className="nav-item">
										<NavLink
											className="nav-link"
											activeClassName="active-link"
											style={isActive(history, "/signin")}
											to="/signin"
										>
											Sign In
								</NavLink>
									</li>
									<li className="nav-item">
										<NavLink
											className="nav-link"
											activeClassName="active-link"
											style={isActive(history, "/signup")}
											to="/signup"
										>
											Sign Up
								</NavLink>
									</li>
								</React.Fragment>
							)}

						{user && user.role === "admin" && (
							<>
								<li className="nav-item">
									<NavLink
										to={`/admin`}
										style={isActive(history, `/admin`)}
										className="nav-link"
										activeClassName="active-link"
									>
										Admin
								</NavLink>
								</li>

								<li className="nav-item">
									<NavLink
										className={
											history.location.pathname === "/users"
												? "active nav-link"
												: "not-active nav-link"
										}
										activeClassName="active-link"
										to="/users"
									>
										Users
								</NavLink>
								</li>

								<li className="nav-item">
									<NavLink
										to={`/findpeople`}
										style={isActive(history, `/findpeople`)}
										className="nav-link"
										activeClassName="active-link"
									>
										Find People
								</NavLink>
								</li>
							</>
						)}

						{user && (
							<React.Fragment>
								<li className="nav-item">
									<NavLink
										to={`/patient/create`}
										style={isActive(history, `/post/create`)}
										className="nav-link"
										activeClassName="active-link"
									>
										Create Patient
								</NavLink>
								</li>

								<li className="nav-item">
									<NavLink
										to={`/findpatients`}
										style={isActive(history, `/findpatients`)}
										className="nav-link"
										activeClassName="active-link"
									>
										Find Patient
								</NavLink>
								</li>

								{/* ADDED */}
								<li className="nav-item">
									<NavLink
										to={`/patientsummary`}
										style={isActive(history, `/patientsummary`)}
										className="nav-link"
										activeClassName="active-link"
									>
										Patient Summary
								</NavLink>
								</li>


								<li className="nav-item">
									<NavLink
										to={`/user/${user._id}`}
										style={isActive(history, `/user/${user._id}`)}
										className="nav-link"
										activeClassName="active-link"
									>
										{console.log(user.fName)}
										{(`${isAuthenticated().user.fName}'s profile`)}
									</NavLink>
								</li>

								<li className="nav-item">
									<span
										className="nav-link"
										activeClassName="active-link"
										style={{ cursor: "pointer", color: "#fff" }}
										onClick={() => signout(() => history.push("/"))}
									>
										Sign Out
								</span>
								</li>
							</React.Fragment>
						)}
					</Nav>
				</Navbar.Collapse>
			</Navbar>

		</>);
};

export default withRouter(Menu);